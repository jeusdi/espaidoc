package io.espaidoc.controller;

public interface RestConstants {

    public interface Endpoints {

        public static final String PATH = "front";
        
        public interface ReferenceEndpoint {

            public static final String PATH = "reference";

            public interface Download {

                public interface Parameters {

                    public static final String ID = "id";
                }

            }

            public interface Upload {

                public interface Parameters {

                    public static final String REFERENCE = "reference";
                    public static final String CONTENT = "content";

                }

            }

        }

    }

    public interface Caches {

        public static final String REFERENCE = "reference";

    }

    public interface Logs {

        public static final String UPLOAD_START = "log.requests.upload.start";

        public static final String CACHE_GET = "log.cache.get";

        public static final String PERSISTENCE_GET = "log.persistence.get";
        public static final String PERSISTENCE_GET_FAIL = "log.persistence.get.fail";

    }

    public interface Faults {

        public interface ParameterNotFound {
            public static final String CODE = "001";
            public static final String MESSAGE = "fault.parameter.not.found";
        }
        
        public interface Sha2Incorrect {
            public static final String CODE = "002";
            public static final String MESSAGE = "fault.sha2.incorrect";
        }

    }

}
