package io.espaidoc.controller.domain;

import java.time.LocalDate;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Check it out.
// @Schema(
//     description = "POJO that represents a reference."
// )
@Data
@NoArgsConstructor
public class ReferencePayload {

    @NotEmpty
	private String transactionId;

	@NotEmpty
	private String codiDirCorp;

	@NotEmpty
	private String title;

	@NotEmpty
	private String name;

	@NotEmpty
    private String description;

    @NotNull
	@Future
	private LocalDate expiration;

    @NotEmpty
    @Pattern(regexp = "S|N")
	private boolean confidential;

	@NotEmpty
	@Pattern(regexp = "S|N")
	private boolean locked;

}
