package io.espaidoc.controller.resources;

import java.io.File;

import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Encoded;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.espaidoc.controller.RestConstants.Endpoints.ReferenceEndpoint;
import io.espaidoc.controller.domain.ReferencePayload;

public interface ReferenceResource {


    /**
     * Returns request reference by id.
     * @param id Reference identity
     * @return
     */
    @GET
    public Response download(
        @PathParam(ReferenceEndpoint.Download.Parameters.ID)
            String id
    );


    /**
     * Ingest requested reference
     * @param reference Attributes
     * @param content File content
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(
        @FormParam(ReferenceEndpoint.Upload.Parameters.REFERENCE)
        @BeanParam
        @Valid
            ReferencePayload reference,
        @FormParam(ReferenceEndpoint.Upload.Parameters.CONTENT)
        @Encoded
            File content
    );

}
