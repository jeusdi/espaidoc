package io.espaidoc.controller.resources;

import java.io.File;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Path;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import io.espaidoc.controller.RestConstants;
import io.espaidoc.controller.RestConstants.Endpoints.ReferenceEndpoint;
import io.espaidoc.controller.config.cdi.annotations.CacheControlConfig;
import io.espaidoc.controller.domain.ReferencePayload;
import io.espaidoc.model.domain.Reference;
import io.espaidoc.service.ReferenceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Path(ReferenceEndpoint.PATH)
public class ReferenceResourceImpl implements ReferenceResource {

    private final transient @CacheControlConfig(maxAge = 20) CacheControl cacheControl;
    private final transient Request request;

    private final transient ReferenceService referenceService;
    // private final transient FrontFaultBuilder faultBuilder;

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(String id) {
        LOG.info(RestConstants.Logs.UPLOAD_START, id);

        return this.referenceService.get(id)
            .map(this::getBuilderOrDefault)
            .get()
            // .orElseThrow(this.faultBuilder.forIllegalArgument().buildSupplier())
            .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response upload(
        @Valid ReferencePayload reference,
        File content
    ) {
        // Response.ok().tag(EntityTag.valueOf(Integer.toString(reference.hashCode())));
        // this.referenceService.create(reference, content);

        return Response.ok().build();
    }

    /**
     * Calculates {@link Reference}'s {@link EntityTag}.
     * @param reference {@link Reference} to look up
     * @return Calculated {@link EntityTag}
     * @throws NullPointerException if {@code reference} is {@code null}
     */
    private EntityTag eTag(
        @NotNull Reference reference
    ) { 
        return EntityTag.valueOf(
            Integer.toString(
                Objects.requireNonNull(reference).hashCode()
            )
        );
    }

    /**
     * Builds a {@link javax.ws.rs.core.Response.ResponseBuilder} using {@code reference} parameter
     * as entity and adding its {@link EntityTag}.
     * @param reference entity
     */
    private ResponseBuilder getDefaultBuilder(
        @NotNull Reference reference
    ) {
        return Response.ok()
            .entity(reference)
            .cacheControl(this.cacheControl)
            .tag(this.eTag(reference));
    }

    /**
     * Builds a {@link javax.ws.rs.core.Response.ResponseBuilder} according
     * to {@code reference}'s {@link EntityTag}.
     * @param reference entity to evaluate
     * @throws NullPointerException if {@code reference} is null
     */
    private ResponseBuilder getBuilderOrDefault(
        @NotNull Reference reference
    ) {
        return Optional.ofNullable(reference)
            .map(this::eTag)
            .map(this.request::evaluatePreconditions)
            .orElse(this.getDefaultBuilder(reference));
    }
}
