package io.espaidoc.controller.config.security;

import java.security.Principal;
import java.util.Collection;

import io.espaidoc.model.domain.User;

public class ApplicationUser implements Principal {

    private final String id;
    private final String subject;
    private final String name;
    private final String firstName;
    private final String lastName;
    private final Collection<String> roles;

    public static ApplicationUser from(User user) {
        return new ApplicationUser(
            user.getId(),
            user.getSubject(),
            user.getName(),
            user.getFirstName(),
            user.getLastName(),
            user.getRoles()
        );
    }

    public ApplicationUser(
        String id,
        String subject,
        String name,
        String firstName,
        String lastName,
        Collection<String> roles
    ) {
        this.id = id;
        this.subject = subject;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roles = roles;
    }


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @return the roles
     */
    public Collection<String> getRoles() {
        return roles;
    }

}
