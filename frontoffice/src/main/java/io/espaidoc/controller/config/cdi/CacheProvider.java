package io.espaidoc.controller.config.cdi;

import java.util.Optional;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.CompleteConfiguration;
import javax.cache.configuration.MutableConfiguration;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;

import io.espaidoc.controller.config.cdi.annotations.ReferenceCacheConfig;
import io.espaidoc.model.domain.Reference;
import io.espaidoc.service.cache.ReferenceCacheLoaderFactory;
import io.espaidoc.service.cache.ReferenceCacheWriterFactory;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CacheProvider {

    private final transient ReferenceCacheLoaderFactory referenceCacheLoaderFactory;
    private final transient ReferenceCacheWriterFactory referenceCacheWriterFactory;

    /**
     * Picks a cache according to {@link ReferenceCacheConfig#name()} attribute.
     * @param injectionPoint Current injection point
     * @return Requested cache
     */
    @Produces
    @ReferenceCacheConfig
    public Cache<String, Reference> getReferenceCache(InjectionPoint injectionPoint) {
        return this.getOrCreateCache(
            this.getNameCache(injectionPoint).orElseThrow(() -> new RuntimeException())
        );
    }

    /**
     * Picks the name of requested cache from current
     * {@link ReferenceCacheConfig} annotation.
     * @param injectionPoint Current injection point
     * @return An {@link Optional} with the name requested cache
     */
    private Optional<String> getNameCache(InjectionPoint injectionPoint) {
        Annotated annotated = injectionPoint.getAnnotated();
        ReferenceCacheConfig referenceCacheAnnotation = annotated.getAnnotation(ReferenceCacheConfig.class);
        return Optional.ofNullable(referenceCacheAnnotation.value())
            .filter(String::isEmpty)
            .filter(String::isBlank);
    }

    /**
     * Picks the requested cache.
     * @param name Requested cache's name
     * @param type Requested cache's value type
     * @return Requested cache
     */
    private Cache<String, Reference> getOrCreateCache(String name) {
        CacheManager cacheManager = Caching.getCachingProvider().getCacheManager();
        Cache<String, Reference> cache = cacheManager.getCache(name, String.class, Reference.class);

        if (cache == null) {
            CompleteConfiguration<String, Reference> config =
                new MutableConfiguration<String, Reference>()
                    .setReadThrough(true)
                    .setCacheLoaderFactory(this.referenceCacheLoaderFactory)
                    .setWriteThrough(true)
                    .setCacheWriterFactory(this.referenceCacheWriterFactory)
                    .setTypes(String.class, Reference.class);

            cache = cacheManager.createCache(name, config);
        }

        return cache;

        // FactoryBuilder.factoryOf(clazz)
    }

}
