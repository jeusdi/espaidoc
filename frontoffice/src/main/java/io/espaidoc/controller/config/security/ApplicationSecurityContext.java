package io.espaidoc.controller.config.security;

import java.security.Principal;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ws.rs.core.SecurityContext;

public class ApplicationSecurityContext implements SecurityContext {

    private transient final Optional<ApplicationUser> user;

    public ApplicationSecurityContext(ApplicationUser user) {
        this.user = Optional.ofNullable(user);
    }

    @Override
    public Principal getUserPrincipal() {
        return this.user.orElse(null);
    }

    @Override
    public boolean isUserInRole(String expectedRole) {
        return this.user
            .map(ApplicationUser::getRoles)
            .filter(Objects::nonNull)
            .map(Collection::stream)
            .orElse(Stream.empty())
            .anyMatch(suggestedRole -> Objects.equals(suggestedRole, expectedRole));
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public String getAuthenticationScheme() {
        return SecurityContext.CLIENT_CERT_AUTH;
    }

}
