package io.espaidoc.controller.config.cdi.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
public @interface ReferenceCacheConfig {

    /**
     * The name of the cache.
     * <p>
     * The name will be used as the key to get jCache cache.
     * </p>
     *
     * @return the name of the cache
     **/
    String value() default "";

}
