package io.espaidoc.controller.config.cdi;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.ws.rs.core.CacheControl;

import io.espaidoc.controller.config.cdi.annotations.CacheControlConfig;

public class RestCacheProvider {

    @Produces
    public CacheControl get(InjectionPoint ip) {
        CacheControlConfig ccConfig = ip.getAnnotated().getAnnotation(CacheControlConfig.class);
        if (ccConfig != null) {
            CacheControl cc = new CacheControl();
            cc.setMaxAge(ccConfig.maxAge());
            cc.setMustRevalidate(ccConfig.mustRevalidate());
            cc.setNoCache(ccConfig.noCache());
            cc.setNoStore(ccConfig.noStore());
            cc.setNoTransform(ccConfig.noTransform());
            cc.setPrivate(ccConfig.isPrivate());
            cc.setProxyRevalidate(ccConfig.proxyRevalidate());
            cc.setSMaxAge(ccConfig.sMaxAge());

            return cc;
        }

        return null;
    }

}
