package io.espaidoc.controller;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@ApplicationPath(RestConstants.Endpoints.PATH)
@OpenAPIDefinition(
    info = @Info(
        title = "Espaidoc - front office", 
        version = "0.0.1"
    )
)
public class FrontOfficeApplication extends Application {

}
