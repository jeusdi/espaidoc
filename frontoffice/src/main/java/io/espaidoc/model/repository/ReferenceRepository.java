package io.espaidoc.model.repository;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import io.espaidoc.model.domain.Reference;

@Repository
public interface ReferenceRepository extends EntityRepository<Reference, String> {

}

