package io.espaidoc.model.domain;

import java.time.Instant;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.deltaspike.data.api.audit.CreatedOn;

public abstract class AuditableEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedOn
    private Instant createdOn;
}