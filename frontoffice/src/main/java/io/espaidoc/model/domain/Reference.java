package io.espaidoc.model.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Reference {

	@EqualsAndHashCode.Include
	@Id
	private String id;

	private String transactionId;
	private String codiDirCorp;
	private String title;
	private String name;
    private String description;
	private LocalDate expiration;
	private boolean confidential;
	private boolean locked;

}
