package io.espaidoc.model.domain;

import java.util.Collection;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class User {

    private String id;
    private String subject;
    private String name;
    private String firstName;
    private String lastName;
    private Collection<String> roles;

}

