package io.espaidoc.faults;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.core.api.message.MessageContext;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@FaultBuilderApi
@Slf4j
@AllArgsConstructor
public class FaultBuilderInvocationHandler implements InvocationHandler {

    private final transient MessageContext messageContext;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LOG.info(
            String.format(
                "Creating ErrorBuilder instance for type '%s'",
                method.getDeclaringClass().getName()
            )
        );

        final FaultBuilderApi errorApi = Objects.requireNonNull(
            method.getDeclaringClass().getAnnotation(FaultBuilderApi.class),
            "Class must be annotated with @FaultBuilderApi"
        );

        final Fault fault = Objects.requireNonNull(
            method.getAnnotation(Fault.class),
            "Method needs to be annotated with @Fault"
        );

        String message = Optional
            .ofNullable(fault.message())
            .map(msg -> this.messageContext.message().template(msg).argumentArray((String[])args))
            .orElse(this.messageContext.message())
            .toString();

        return new FaultBuilderImpl()
            .withModule(errorApi.module())
            .withCode(fault.code())
            .withPrefix(errorApi.errorPrefix())
            .withMessage(message);
    }
}