package io.espaidoc.faults;

import org.apache.deltaspike.partialbean.api.PartialBeanBinding;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@PartialBeanBinding
public @interface FaultBuilderApi {

    String module() default "";

    String errorPrefix() default "ERROR";

}
