package io.espaidoc.faults;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import io.espaidoc.faults.exceptions.FaultException;

public class FaultBuilderImpl implements FaultBuilder {

    private static final String PATTERN_MESSAGE = "%s-%s-%s %s";

    private transient String code;
    private transient String module;
    private transient String prefix;
    private transient String message;
    private transient Throwable cause;

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultBuilder withCause(Throwable cause) {
        this.cause = cause;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultBuilder withModule(String module) {
        this.module = Objects.requireNonNull(module, "Cannot set null module");
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultBuilder withPrefix(String errorPrefix) {
        this.prefix = Objects.requireNonNull(errorPrefix, "Cannot set null errorPrefix");
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultBuilder withCode(String code) {
        this.code = Objects.requireNonNull(code, "Cannot set null error code");
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultBuilder withMessage(String message) {
        this.message = Objects.requireNonNull(message, "Cannot add null message");
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultException build() {
        return FaultException.builder()
            .code(this.code)
            .message(this.createMessage())
            .cause(this.cause)
            .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Supplier<FaultException> buildSupplier() {
        return () -> this.build();
    }

    /**
     * Builds the message according to its field values.
     * @return formatted message
     */
    private String createMessage() {
        Object[] args = Stream.of(
            this.module,
            this.prefix,
            this.code,
            this.message
        )
        .map(Optional::ofNullable)
        .map(msg -> msg.orElse("undefined"))
        .toArray();
        
        return String.format(PATTERN_MESSAGE, args);
    }

}

