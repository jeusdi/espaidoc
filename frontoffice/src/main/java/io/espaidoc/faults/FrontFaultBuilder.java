package io.espaidoc.faults;

import javax.enterprise.context.ApplicationScoped;

import io.espaidoc.controller.RestConstants.Faults;

import java.io.Serializable;

@ApplicationScoped
@FaultBuilderApi(module = "FRONT", errorPrefix = "ERR-")
public interface FrontFaultBuilder extends Serializable {

    @Fault(
        code = Faults.ParameterNotFound.CODE,
        message = Faults.ParameterNotFound.MESSAGE
    )
    FaultBuilder forParameterNotFound();

    @Fault(
        code = Faults.Sha2Incorrect.CODE,
        message = Faults.Sha2Incorrect.MESSAGE
    )
    FaultBuilder forIllegalArgument();

}

