package io.espaidoc.faults;

import java.util.function.Supplier;

import io.espaidoc.faults.exceptions.FaultException;

public interface FaultBuilder {

    FaultBuilder withCause(Throwable cause);

    FaultBuilder withModule(String module);
    FaultBuilder withPrefix(String errorPrefix);
    FaultBuilder withCode(String code);
    FaultBuilder withMessage(String message);

    FaultException build();
    Supplier<FaultException> buildSupplier();
}