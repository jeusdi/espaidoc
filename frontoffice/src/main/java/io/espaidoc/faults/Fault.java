package io.espaidoc.faults;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Fault {

    String code();

    String message();

}

