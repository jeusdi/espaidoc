package io.espaidoc.faults.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;

@AllArgsConstructor
public class FaultException extends RuntimeException {

    private static final long serialVersionUID = 8841797257142927363L;

    private final transient String code;

    public FaultException(String code, String message) {
        super(message);
        
        this.code = code;
    }

    @Builder
    public FaultException(String code, String message, Throwable cause) {
        super(message, cause);
        
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
