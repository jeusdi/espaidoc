package io.espaidoc.service;

import java.io.File;
import java.util.Optional;

import io.espaidoc.model.domain.Reference;

public interface ReferenceService {

    /**
     * Obtains a {@link Reference} using an id.
     * @param id {@link Reference} identifier
     * @return {@link Reference} entity represented by its identifier
     */
    public Optional<Reference> get(String id);

    /**
     * Stores a {@link Reference}.
     * @param reference {@link Reference} to store
     * @param content Content of reference
     */
    public void create(Reference reference, File content);

}