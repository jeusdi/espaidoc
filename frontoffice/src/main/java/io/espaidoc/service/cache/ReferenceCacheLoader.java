package io.espaidoc.service.cache;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.cache.integration.CacheLoader;
import javax.cache.integration.CacheLoaderException;

import io.espaidoc.model.domain.Reference;
import io.espaidoc.model.repository.ReferenceRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReferenceCacheLoader implements CacheLoader<String, Reference> {

    private final transient ReferenceRepository referenceRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public Reference load(String key) throws CacheLoaderException {
        return this.referenceRepository.findBy(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Reference> loadAll(Iterable<? extends String> keys) throws CacheLoaderException {
        return StreamSupport.stream(keys.spliterator(), false)
            .collect(Collectors.toMap(Function.identity(), this::load));
    }

}
