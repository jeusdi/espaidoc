package io.espaidoc.service.cache;

import java.util.Collection;
import java.util.Objects;

import javax.cache.Cache.Entry;
import javax.cache.integration.CacheWriter;
import javax.cache.integration.CacheWriterException;

import io.espaidoc.model.domain.Reference;
import io.espaidoc.model.repository.ReferenceRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReferenceCacheWriter implements CacheWriter<String, Reference> {

    private final transient ReferenceRepository referenceRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(Entry<? extends String, ? extends Reference> entry) throws CacheWriterException {
        this.referenceRepository.save(entry.getValue());
        // this.entityManager.merge(entry.getValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeAll(Collection<Entry<? extends String, ? extends Reference>> entries) throws CacheWriterException {
        entries.forEach(this::write);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Object key) throws CacheWriterException {
        Reference reference = this.referenceRepository.findBy((String)key);

        if (Objects.nonNull(reference)) {
            this.referenceRepository.remove(reference);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAll(Collection<?> keys) throws CacheWriterException {
        keys.forEach(this::delete);
    }

}
