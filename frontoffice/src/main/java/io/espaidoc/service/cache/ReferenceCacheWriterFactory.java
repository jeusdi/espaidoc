package io.espaidoc.service.cache;

import javax.cache.configuration.Factory;

import io.espaidoc.model.repository.ReferenceRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReferenceCacheWriterFactory implements Factory<ReferenceCacheWriter> {

    private static final long serialVersionUID = 96360408468164108L;

    private final transient ReferenceRepository referenceRepository;

    @Override
    public ReferenceCacheWriter create() {
        return new ReferenceCacheWriter(this.referenceRepository);
    }

}
