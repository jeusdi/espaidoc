package io.espaidoc.service.cache;

import javax.cache.configuration.Factory;

import io.espaidoc.model.repository.ReferenceRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReferenceCacheLoaderFactory implements Factory<ReferenceCacheLoader> {

    private static final long serialVersionUID = 6299876544858755727L;

    private final transient ReferenceRepository referenceRepository;

    @Override
    public ReferenceCacheLoader create() {
        return new ReferenceCacheLoader(this.referenceRepository);
    }

}
