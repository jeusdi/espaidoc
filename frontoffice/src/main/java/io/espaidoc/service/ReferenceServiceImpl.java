package io.espaidoc.service;

import java.io.File;
import java.util.Optional;

import javax.cache.Cache;

import io.espaidoc.model.domain.Reference;
import lombok.AllArgsConstructor;

/**
 * Service-like implementation intended to handle {@link Reference} operations.
 */
@AllArgsConstructor
public class ReferenceServiceImpl implements ReferenceService {

    private final transient Cache<String, Reference> cache;

    /**
     * {@inheritDoc}
     */
    public Optional<Reference> get(String id) {
        return Optional
            .ofNullable(id)
            .map(this.cache::get);
    }

    /**
     * {@inheritDoc}
     */
    public void create(Reference reference, File content) {
        // this.cache.putIfAbsent("key???", reference);
    }

}
