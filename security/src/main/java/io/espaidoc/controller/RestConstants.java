package io.espaidoc.controller;

public interface RestConstants {

    public interface Resources {

        public interface User {

            public static final String PATH = "user";

            public interface Token {

                public interface Parameters {

                    public static final String ID = "id";
                }

            }

        }

    }

    public interface Logs {

    }

}
