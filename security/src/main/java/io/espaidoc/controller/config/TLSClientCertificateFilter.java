package io.espaidoc.controller.config;

import java.io.IOException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.annotation.Priority;
import javax.servlet.ServletContext;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response.Status;

import io.espaidoc.service.UserService;

/**
* A ContainerRequestFilter to do certificate validation beyond the tls validation.
* For example, the filter matches the subject against a regex and will 403 if it doesn't match
*
* @author <a href="mailto:wdawson@okta.com">wdawson</a>
*/
@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class TLSClientCertificateFilter implements ContainerRequestFilter {

    private static final String X509_CERTIFICATE_ATTRIBUTE = "javax.servlet.request.X509Certificate";

    private transient final ServletContext requestContext;
    private transient final Pattern dnRegex;
    private transient final UserService userService;

    /**
     * Constructor for the CertificateValidationFilter.
     *
     * @param requestContext Current servlet context in order to pick client
     * certificate
     *
     * @param dn The regular expression to match subjects of certificates with.
     *                E.g.: "^CN=service1\.example\.com$"
     */
    public TLSClientCertificateFilter(
        @Context ServletContext requestContext,
        @Dn String dn,
        UserService userService
    ) {
        this.requestContext = requestContext;
        this.dnRegex = Pattern.compile(dn);
        this.userService = userService;
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        this.pickCertificate()
            .flatMap(this::pickIdentity)
            .map(Principal::getName)
            .map(this.userService::get)
            .orElseThrow(() -> new WebApplicationException(Status.UNAUTHORIZED));
    }

    /**
     * Picks client certificate
     */
    private Optional<X509Certificate> pickCertificate() {
        Object attribute = this.requestContext.getAttribute(X509_CERTIFICATE_ATTRIBUTE);

        return Optional.ofNullable(attribute)
            .map(X509Certificate[].class::cast)
            .map(Stream::of)
            .orElse(Stream.empty())
            .findFirst();
    }

    private Optional<Principal> pickIdentity(final X509Certificate certificate) {
        // String clientCertDN = certificate.getSubjectDN().getName();
        // if (!dnRegex.matcher(clientCertDN).matches()) {
            // TODO: Throw exception and log it
            // containerRequestContext.abortWith(buildForbiddenResponse("Certificate subject is not recognized!"));
        // }

        return Optional.ofNullable(certificate.getSubjectX500Principal());
    }

}
