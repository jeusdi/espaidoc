package io.espaidoc.controller.resources;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import io.espaidoc.controller.RestConstants;

@Path(RestConstants.Resources.User.PATH)
public class ApplicationUserResourceImpl implements ApplicationUserResource {

    @Override
    public Response getToken() {
        return null;
    }

}
