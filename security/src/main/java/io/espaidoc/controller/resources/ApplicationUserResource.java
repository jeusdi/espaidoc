package io.espaidoc.controller.resources;

import javax.ws.rs.GET;
import javax.ws.rs.core.Response;

public interface ApplicationUserResource {

    @GET
    Response getToken();

}
